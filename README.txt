
Role ownership module allows to set the role as the owner (to have permissions
to read, write, delete) of the node. Users can 'pass' ownership between groups.

-- Configuration --
You need to select content types that role ownership access rights to apply.

By default users can set up only the role they are assigned to. You can use
permission "allow select all roles" to allow users set up any available role.
