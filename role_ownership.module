<?php

/**
 * Implementation of hook_help().
 */
function role_ownership_help($path, $arg) {
  switch ($path) {
    // Main module help for the block module
    case 'admin/help#role_ownership':
      return '<p>' . t('Access module. Allows to set ownership access rights (read, write, delete) to specified group. By default user can set up the role he is assigned to. There is permission "allow select all roles" in order to allow users to select any available role for the node.') . '</p>';
  }
}

/**
 * Implementation of hook_menu().
 */
function role_ownership_menu() {
  $items = array();

  $items['admin/settings/role_ownership'] = array(
    'title' => 'Role Ownership',
    'description' => "Edit content types .",
    'page callback' => 'drupal_get_form',
    'page arguments' => array('role_ownership_form_admin'),
    'access arguments' => array('administer nodes'),
    'file' => 'role_ownership.admin.inc',
  );

  return $items;
}

/**
 * Implementation of hook_node_grants().
 */
function role_ownership_node_grants($account, $op) {

  $grants = array();
  foreach ($account->roles as $role_id => $role_name) {
    $grants['role_ownership'][] = $role_id;
  }

  if (!empty($grants)) {
    return $grants;
  }
  return NULL;
}

/**
 * Implementation of hook_node_access_records().
 */
function role_ownership_node_access_records($node) {
  // Check if node is has type that we apply role_ownership access.
  $role_ownership_node_types = variable_get('role_ownership_node_types', array());

  if (empty($role_ownership_node_types[$node->type])) {
    return;
  }

  if ((!isset($node->role_ownership_role)) || (empty($node->role_ownership_role))) {
    return;
  }

  return array(array(
    'realm' => 'role_ownership',
    'gid' => $node->role_ownership_role,
    'grant_view' => TRUE,
    'grant_update' => TRUE,
    'grant_delete' => TRUE,
    'priority' => 0,
  ));
}

/**
 * Implementation of hook_form_alter().
 */
function role_ownership_form_alter(&$form, &$form_state, $form_id) {
  // Check whether it is node edit form.
  if (strpos($form_id, '_node_form') === FALSE) {
    return;
  }

  $role_ownership_node_types = variable_get('role_ownership_node_types', array());
  // Check if the node type is the one chosen in settings.
  if (empty($role_ownership_node_types[$form['#node']->type])) {
    return;
  }

  $form['role_ownership'] = array(
    '#type' => 'fieldset',
    '#title' => t('Role Ownership'),
  );

  // Check if user has right to select any ownership role.
  if (user_access('allow select all roles')) {
    $roles = user_roles();
  }
  else {
    global $user;
    $roles = $user->roles;
  }
  $form['role_ownership']['role_ownership_role'] = array(
    '#title' => t('Please select owner role'),
    '#type' => 'select',
    '#options' => $roles,
    '#default_value' => $form['#node']->role_ownership_role,
  );
}

/**
 * Implementation of hook_nodeapi().
 */
function role_ownership_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  $role_ownership_node_types = variable_get('role_ownership_node_types', array());
  // Check if the node type is the one chosen in settings.
  if (empty($role_ownership_node_types[$node->type])) {
    return;
  }

  switch ($op) {
    case 'load':
      $node->role_ownership_role = db_result(db_query('SELECT rid FROM {role_ownership} WHERE nid=%d', $node->nid));
      break;
    case 'insert':
      $object = array('nid' => $node->nid, 'rid' => $node->role_ownership_role);
      drupal_write_record('role_ownership', $object);
      break;
    case 'update':
      $object = array('nid' => $node->nid, 'rid' => $node->role_ownership_role);
      // Check if this node already has records in {role_ownership} table or not.
      // If not we need to insert new record instead of update.
      $previous_rid = db_result(db_query('SELECT rid FROM {role_ownership} WHERE nid=%d', $node->nid));
      if (empty($previous_rid)) {
        drupal_write_record('role_ownership', $object);
      }
      else {
        drupal_write_record('role_ownership', $object, 'nid');
      }
      break;
    case 'delete':
      db_query('DELETE FROM {role_ownership} WHERE nid=%d', $node->nid);
      break;
  }
}

/**
 * Implementation of hook_perm().
 */
function role_ownership_perm() {
  return array('allow select all roles');
}
