<?php

/**
 * @file
 *
 * Admin settings form
 */

/**
 * Generate form for admin settings
 */
function role_ownership_form_admin() {
  $form = array();

  $content_types = content_types();
  $options = array();
  foreach ($content_types as $type) {
    $options[$type['type']] = $type['name'];
  }

  $form['role_ownership_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Select content types to apply Role Ownership access rights.'),
    '#options' => $options,
    '#default_value' => variable_get('role_ownership_node_types', array()),
  );

  if (module_exists('role_ownership_log')) {
    $form['role_ownership_log_limit'] = array(
      '#type' => 'textfield',
      '#title' => t('How many records to show in log'),
      '#default_value' => variable_get('role_ownership_log_limit', 10),
    );
  }

  return system_settings_form($form);
}