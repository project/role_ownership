
Role ownership log module allows to keep tracking how ownership role has
been changing.

-- Configuration --
You can set up how many log records to show. By default you can see 10 records
in edit form of the node.
